import { createContext, useState } from "react";

export const AppContext = createContext(null);

function AppContextProvider({ children }) {
  const [animateSymbol, setAnimateSymbol] = useState();

  return (
    <AppContext.Provider value={{ animateSymbol, setAnimateSymbol }}>
      {children}
    </AppContext.Provider>
  );
}

export default AppContextProvider;