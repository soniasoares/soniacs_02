import Head from 'next/head';
import 'normalize.css/normalize.css';
import '@/styles/globals.sass';
import AppContextProvider from "../context/appContext";
import Script from 'next/script';

export default function App({ Component, pageProps }) {
  return (
    <AppContextProvider>
      <Component {...pageProps} />
    </AppContextProvider>
  );
}

const Analytics = () => {
  return (
    <>
      <Script strategy="afterInteractive" src="https://www.googletagmanager.com/gtag/js?id=G-RJLQ2BV0TM"/>
      <Script
        id='google-analytics'
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{ __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-RJLQ2BV0TM');
        `,}}
      />
    </>
  );
}
