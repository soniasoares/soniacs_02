import Link from 'next/link';
import Head from 'next/head';
import { siteTitle } from '@/components/layout';
import Favicon from '@/components/favicon';
import AboutAnimation from '@/components/about/aboutAnimation';

export default function FourOhFour() {
  return (
    <>
      <Head>
        <Favicon />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>{`${siteTitle} | frontend developer`}</title>
      </Head>
      <main role="main" className='error-page'>
        <AboutAnimation reverse={true} />
        <h1>Page Not Found</h1>
        <p>I'm sorry but it seems that you're lost. <br />
        Contact me at freesonia[at]gmail[.]com or <Link href="/">go back home</Link>.</p>
      </main>
    </>
  )
}