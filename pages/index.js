import Head from 'next/head';
import Layout, { siteTitle } from '@/components/layout';
import About from '@/components/about';
import Skills from '@/components/skills';
import Projects from '@/components/projects';
import { getAllArticles, getArticle } from '../lib/content';

export async function getStaticProps() {
  const aboutContent = await getArticle('', 'about');
  const personalProjects = await getAllArticles('/personalProjects');
  const professionalProjects = await getAllArticles('/professionalProjects');
  
  return {
    props: {
      aboutContent,
      personalProjects,
      professionalProjects
    },
  };
}

const Home = ({aboutContent, personalProjects, professionalProjects}) => {
  return (
    <Layout>
      <Head>
        <title>{`${siteTitle} | frontend developer`}</title>
      </Head>
      <About content={aboutContent} />
      <Skills />
      <Projects title="Personal Projects" projects={personalProjects} />
      <Projects title="Professional Projects" projects={professionalProjects} />
    </Layout>
  )
}

export default Home;
