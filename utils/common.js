// CLASS UTILS
export const addClass = (elem, className) => {
  if(!elem.classList.contains(className)) {
    elem.classList.add(className);
  }
};
export const removeClass = (elem, className) => {
  if(elem.classList.contains(className)) {
    elem.classList.remove(className);
  }
};