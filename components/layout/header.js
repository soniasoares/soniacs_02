import { AppContext } from "../../context/appContext";
import { useContext } from "react";

const Header = () => {
  const { setAnimateSymbol } = useContext(AppContext);

  const menu = [
    {
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/sonia-soares-frontend'
    },
    {
      name: '<del>github</del>\/codeberg',
      url: 'https://codeberg.org/soniasoares'
    }
  ];

  const parseHtmlText = (txt) => {
    const parser = new DOMParser();
    //return parser.parseFromString(txt, 'text/html');
    console.log(parser.parseFromString(txt, 'text/html'))
    return txt;
  };

  return (
    <header className="app-header">
      <ul className="main-menu">
        {menu.map((link, index) => <li key={index}>
          <a 
            href={link.url} 
            target="_blank" 
            rel="noopener" 
            onMouseEnter={() => setAnimateSymbol(true)}
            onMouseLeave={() => setAnimateSymbol(false)}
          ><span dangerouslySetInnerHTML={{ __html: link.name }} /></a>
        </li>)}
      </ul>
    </header>
  );
};

export default Header;