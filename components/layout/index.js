import Head from 'next/head';
import Header from './header';
import Footer from './footer';
import ScrollTop from '../scrollTop';
import Favicon from '../favicon';

export const siteTitle = 'soniacs';

const Layout = ({ children }) => {
  return (
    <>
      <Head>
        <Favicon />
        <meta name="description" content="Frontend developer website" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Header />
      <main role="main" className='app-main'>{children}</main>
      <ScrollTop />
      <Footer />
    </>
  );
};

export default Layout;