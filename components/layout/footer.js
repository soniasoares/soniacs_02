const Footer = () => {
  const date = new Date();
  return (
    <footer className="app-footer">
      <p>&copy;{date.getFullYear()} Sonia Soares. Built with <a href="https://nextjs.org/">NextJS</a> and using <a href="https://ionic.io/ionicons/">ionicons</a>.</p>
    </footer>
  );
};

export default Footer;