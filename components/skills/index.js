import {
  IoLogoHtml5, 
  IoLogoCss3,
  IoLogoSass,
  IoLogoJavascript,
  IoLogoReact,
  IoLogoVue,
  IoLogoAngular,
  IoLogoIonic } from "react-icons/io5";
import ShowElem from "../showElem";


const Skills = () => {
  const skills = [
    {name: 'HTML', icon:<IoLogoHtml5 />},
    {name: 'CSS', icon:<IoLogoCss3 />},
    {name: 'SASS', icon:<IoLogoSass />},
    {name: 'Javascript', icon:<IoLogoJavascript />},
    {name: 'React', icon:<IoLogoReact />},
    {name: 'Vue', icon:<IoLogoVue />},
    {name: 'Angular 2+', icon:<IoLogoAngular />},
    {name: 'Ionic', icon:<IoLogoIonic />},
  ];

  return (
    <ShowElem>
      <section className="skills-section">
          <h2>Skills</h2>
          <ul>
            {skills.map((skill,index) => (
              <li key={index}>
                {skill.icon && <span className="icon">{skill.icon}</span>} 
                <span className="name">{skill.name}</span>
              </li>
            ))}
          </ul>
      </section>
    </ShowElem>
  )
};

export default Skills;