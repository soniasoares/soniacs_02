import { useRef, useLayoutEffect } from 'react';
import { addClass } from '@/utils/common';
import styles from './ShowElem.module.sass';

const ShowElem = ({children}) => {
  const container = useRef(null);

  useLayoutEffect(() => {
    const observer = new IntersectionObserver(([entry]) => {
      if(entry.isIntersecting) {
        addClass(entry.target, styles.visible);
      }
    }, { 
      rootMargin: '0px 0px -100px',
    });

    observer.observe(container.current);
    return () => {
      observer.disconnect();
    };
  });

  return (
    <div className={styles.container} ref={container}>
      {children}
    </div>
  );
}

export default ShowElem;