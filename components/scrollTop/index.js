import { useState, useEffect } from "react";
import { IoIosArrowRoundUp } from "react-icons/io";

const ScrollTop = () => {
  const [showBtn, setShowBtn] = useState(false);

  useEffect(() => {
    const handleScroll = e => {
      let scroll = e.target.documentElement.scrollTop;
      if(scroll > 100) {
        setShowBtn(prevState => !prevState ? true : prevState);
      } else {
        setShowBtn(prevState => prevState ? false : prevState);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  },[]);

  const handleClick = () => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
  };

  return (
    <div className={`scroll-top ${showBtn ? 'visible' : ''}`} onClick={handleClick}><IoIosArrowRoundUp /></div>
  );
};

export default ScrollTop;