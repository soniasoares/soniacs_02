import { useRef, useEffect } from 'react';
import ShowElem from "../showElem";

const Projects = ({title, projects}) => {
  return (
    <ShowElem>
      <section className="projects-sections">
        <h2>{title}</h2>
        <ul>
          {projects && projects.sort((a,b) => ('order' in b) - ('order' in a) || b.order - a.order).map((project,index) => (
            <ProjectsItem key={index} project={project} />
          ))}
        </ul>
      </section>
    </ShowElem>
  );
};

export default Projects;

const ProjectsItem = ({project}) => {
  const itemRef = useRef(null)

  useEffect(() => {
    const anchors = itemRef.current.getElementsByTagName('a');
    for(let i = 0; i < anchors.length; i++) {
      anchors[i].setAttribute('target', '_blank')
      anchors[i].setAttribute('rel', "noopener");
    }    
  }, []);

  return (
    <li className="project-item" ref={itemRef}>
      <ShowElem>
        <h3>{project.title}</h3>
        {project.date && <div className="date">{project.date}</div>}
        {project.contentHtml && <div dangerouslySetInnerHTML={{ __html: project.contentHtml }} />}
        {project.url && <a href={project.url}>Website</a>}
        {project.tasks && <div className="tasks"><strong>Tasks:</strong> {project.tasks}</div>}
      </ShowElem>
    </li>
  );
};