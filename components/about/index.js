import AboutAnimation from "./aboutAnimation";

const About = ({content}) => {
  return (
    <section className="about">
      <AboutAnimation />
      <header className="about-header">
        <h1>{content.title}</h1>
      </header>
      <div className="about-text" dangerouslySetInnerHTML={{ __html: content.contentHtml }} />
    </section>
  )
}

export default About;
