import { AppContext } from "../../context/appContext";
import { useContext } from "react";

const AboutAnimation = ({reverse}) => {
  const { animateSymbol } = useContext(AppContext);

  return (
    <div className={`about-animation ${animateSymbol && 'animate'} ${reverse && 'reverse'}`}>
      <span className="whiskers left">(=</span><span className="eyes">^</span>.<span className="eyes">^</span><span className="whiskers right">=)</span>
    </div>
  );
}

export default AboutAnimation;