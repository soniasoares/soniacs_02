---
title: Billing Manager
date: 2022-current
url: https://codeberg.org/soniasoares/billingmanager
tasks: UX Design and Frontend development (CSS/SASS, Vue)
order: 10
---
Web app to manage multi-user monthly bills and deposits.