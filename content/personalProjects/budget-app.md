---
title: Budget App
date: 2023
url: https://codeberg.org/soniasoares/budget-app
tasks: UX Design and Frontend development (CSS/SASS, React, Firebase)
order: 20
---
Monthly budget management web app.