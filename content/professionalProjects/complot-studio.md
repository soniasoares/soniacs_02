---
title: Complot Studio
date: 2019
url: https://www.complotstudio.pt
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 120
---
Website developed at Queo Lda.