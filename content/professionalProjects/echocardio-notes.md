---
title: Echocardio Notes
date: 2021-2022
url: https://play.google.com/store/apps/details?id=pt.queo.ecocardiografia
tasks: Frontend development with CSS/SASS, Javascript and Ionic/Angular.
order: 200
---
Echocardiography scientific manual app for smartphone and tablet. Developed at Queo Lda with Ionic.