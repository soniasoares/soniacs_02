---
title: jawesome.dev
date: 2023-current
url: https://www.jawesome.dev/
tasks: Identity, UX Design and Frontend development with CSS/SASS, Javascript, React and NextJs
order: 230
---
Website for a web development company.