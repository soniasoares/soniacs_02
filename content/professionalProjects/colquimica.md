---
title: Colquímica
date: 2017-2020
url: https://www.colquimica.com
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 70
---
Website developed at Queo Lda.