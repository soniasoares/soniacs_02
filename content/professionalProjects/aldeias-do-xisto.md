---
title: Aldeias do Xisto
date: 2021-2023
url: https://www.aldeiasdoxisto.pt
tasks: Frontend development with CSS/SASS, Django templates, JavaScript, JQuery and Vue
order: 220
---
Website developed at Queo Lda.