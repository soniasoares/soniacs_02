---
title: Nomad
date: 2017-2020
url: https://www.nomad.pt
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 60
---
Website developed at Queo Lda.