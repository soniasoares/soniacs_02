---
title: Events App
date: 2017-2022
tasks: Frontend development with Javascript, Ionic/Angular and CSS/SASS
order: 80
---
Smartphone apps built with Ionic/Angular and Python-Django to manage events communications. Used in Milhões de Festa and Tremor. Developed at Queo Lda.