---
title: Women in Racing
date: 2018-2022
url: https://www.womeninracing.co.uk
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 100
---
Website developed at Queo Lda.