---
title: RAR Group
date: 2020-2022
url: https://www.rar.com
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 150
---
Website developed at Queo Lda.