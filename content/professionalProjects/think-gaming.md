---
title: Think Gaming
date: 2013-2015
tasks: UX Design and frontend development with CSS/LESS, Django Templates, JavaScript, JQuery
order: 10
---
UX design, frontend development and maintenance for Think Gaming, a startup that provided monetization data about mobile games. My work involved developing the web site and the backoffice for subscribed users.