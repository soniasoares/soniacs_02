---
title: Mark Lemanski
date: 2021 – 2022
url: https://www.marklemanski.com/
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 180
---
Website developed at Queo Lda.