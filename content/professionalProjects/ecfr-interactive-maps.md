---
title: ECFR Interactive Maps
date: 2018-2021
tasks: Frontend development with CSS/SASS, HTML, JavaScript, SVG, Snap.js and Chart.js
order: 91
---
SVG interactive maps made for ECFR.\
[Mapping European Leverage in the Mena Region](https://ecfr.eu/special/mapping_eu_leverage_mena/)\
[The Middle List New Battle Lines](https://ecfr.eu/special/battle_lines)\
[Mapping African Regional Cooperation](https://ecfr.eu/special/african-cooperation/)\
[Mapping Armed Groups in Mali and the Sahel](https://ecfr.eu/special/sahel_mapping/)