---
title: Objectif
date: 2015
url: https://www.objectif.co.uk
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 20
---
Website developed at Queo Lda.