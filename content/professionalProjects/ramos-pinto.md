---
title: Ramos Pinto
date: 2020-2022
url: https://www.ramospinto.pt
tasks: Frontend development with CSS/SASS, Django Templates, JavaScript, JQuery
order: 140
---
Website developed at Queo Lda.