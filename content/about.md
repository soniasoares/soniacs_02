---
title: Hello, there! I’m Sonia Soares.
---
I’m a frontend developer from Portugal with a background in graphic design. I started working as a UI/UX designer and then transitioned into web development. I have over 10 years of professional experience working with HTML, CSS/SASS and Javascript, building websites and web apps.

Currently, I’m mostly doing things with React but I have previously worked with Vue, Angular and jQuery. I've also done some basic things with PHP and Django.

If you’d like to work with me or just say hi, please contact me at freesonia[at]gmail[.]com

You can also scroll down to see some of my personal and professional projects.