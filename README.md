# My Website

Built with [Next.js](https://nextjs.org/) and its static site generator functionality.

http://soniacs.com

## Project Setup
	cd soniacs_02
	npm install

## Run Server
	npm run dev

## Generate Static Site
	npm run build

## Static Files Folder
    /dist