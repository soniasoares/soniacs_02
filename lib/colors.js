export const colors = {
  spring: ['#e2e3e1', '#299e7e'],
  summer: ['#e2e3e1', '#E0401F'],
  autumn: ['#e2e3e1', '#AB5825'],
  winter: ['#e2e3e1', '#393b50']
};  

export const setColor = (textColor, bodyBg) => {
  document.documentElement.style.setProperty('--text-color', textColor);
  document.documentElement.style.setProperty('--body-bg', bodyBg);
}