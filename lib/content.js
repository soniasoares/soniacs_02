import fs from 'fs'; // read files from the file system
import path from 'path'; // manipulate file paths
import matter from 'gray-matter'; // parse the metadata in each markdown file
import { remark } from 'remark'; //render markdown content
import html from 'remark-html'; //render markdown content

const contentDirectory = path.join(process.cwd(), 'content');

export async function getAllArticles(directory) {
  // Get file names under /posts
  const fileNames = fs.readdirSync(contentDirectory + directory);
  const allArticles = fileNames.map(async (fileName) => {
    // Remove ".md" from file name to get id
    const id = fileName.replace(/\.md$/, '');

    // Read markdown file as string
    const fullPath = path.join(contentDirectory + directory, fileName);
    const fileContents = fs.readFileSync(fullPath, 'utf8');

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Use remark to convert markdown into HTML string
    const processedContent = await remark()
      .use(html)
      .process(matterResult.content);
    const contentHtml = processedContent.toString();

    // Combine the data with the id
    return {
      id,
      contentHtml,
      ...matterResult.data,
    };
  });
  // Return all articles
  return Promise.all(allArticles);
}

export async function getArticle(directory, id) {
  const fullPath = path.join(contentDirectory + directory, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');

  // Use gray-matter to parse the post metadata section
  const matterResult = matter(fileContents);

  // Use remark to convert markdown into HTML string
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  // Combine the data with the id and contentHtml
  return {
    id,
    contentHtml,
    ...matterResult.data,
  };
}